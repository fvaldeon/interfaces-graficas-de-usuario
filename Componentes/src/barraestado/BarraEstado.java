package barraestado;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class BarraEstado extends JPanel{

	private static final long serialVersionUID = 1L;
	private JLabel mensajeInformacion;
	private JLabel mensajeError;
	
	
	public BarraEstado() {
		
		mensajeError = new JLabel();
		mensajeInformacion = new JLabel();
		mensajeError.setForeground(Color.RED);
		mensajeInformacion.setForeground(Color.GREEN);
		
		setLayout(new BorderLayout());
		
		add(mensajeInformacion, BorderLayout.WEST);
		add(mensajeError, BorderLayout.EAST);
	}
	
	public void setMensajeError(String mensaje) {
		mensajeError.setText(mensaje);
	}
	
	public void setMensajeInfo(String mensaje) {
		mensajeInformacion.setText(mensaje);
	}

}
