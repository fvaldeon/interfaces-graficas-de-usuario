package gui.componentes;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import base.Coche;
import base.Conductor;
import gui.Modelo;

import java.awt.GridLayout;
import javax.swing.JList;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class DialogoConductores extends JDialog {

	private static final long serialVersionUID = 1L;
	
	private final JPanel contentPanel = new JPanel();
	private JList<Conductor> listConductores;
	private JList<Coche> listCoches;
	private DefaultListModel<Conductor> dlmConductores;
	private DefaultListModel<Coche> dlmCoches;
	private Modelo modelo;

	private DialogoConductores(Modelo modelo) {
		this.modelo = modelo;
		
		initUI();
		
		dlmCoches = new DefaultListModel<Coche>();
		dlmConductores = new DefaultListModel<Conductor>();
		
		listCoches.setModel(dlmCoches);
		listConductores.setModel(dlmConductores);
		
		listarConductores();
		
		setModal(true);
	}

	
	private void listarConductores() {
		
		for(Conductor conductor : modelo.getConductores()) {
			dlmConductores.addElement(conductor);
		}
	}
	
	protected void mostrarCochesDeConductor(Conductor conductor) {
		
		dlmCoches.clear();
		
		for(Coche coche : modelo.getCoches()) {
			if(coche.getConductor() == conductor) {
				dlmCoches.addElement(coche);
			}
		}

	}

	protected void botonCerrar() {
	
		this.dispose();
	}
	
	
	
	private void initUI() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(2, 0, 0, 0));
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel);
			{
				JLabel lblNewLabel = new JLabel("Conductores:");
				panel.add(lblNewLabel);
			}
			{
				JScrollPane scrollPane = new JScrollPane();
				panel.add(scrollPane);
				{
					listConductores = new JList<Conductor>();
					listConductores.addListSelectionListener(new ListSelectionListener() {
						public void valueChanged(ListSelectionEvent arg0) {
							Conductor conductor = listConductores.getSelectedValue();
						
							mostrarCochesDeConductor(conductor);
						}
					});
					scrollPane.setViewportView(listConductores);
				}
			}
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel);
			{
				JLabel lblNewLabel_1 = new JLabel("Vehiculos:");
				panel.add(lblNewLabel_1);
			}
			{
				JScrollPane scrollPane = new JScrollPane();
				panel.add(scrollPane);
				{
					listCoches = new JList<Coche>();
					scrollPane.setViewportView(listCoches);
				}
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Cerrar");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						
						botonCerrar();
						
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}
	
	public static void mostrarDialogo(Modelo modelo) {
		
		DialogoConductores dialogo = new DialogoConductores(modelo);
		dialogo.setVisible(true);
		
		dialogo.setLocationRelativeTo(null);
	}

}
