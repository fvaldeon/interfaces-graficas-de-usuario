package pruebacolorchooser;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPanel panel;
	private JButton btnNewButton;
	private JPanel panel_2;
	private JPanel panel_1;
	private JButton btnNewButton_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		btnNewButton = new JButton("New button");
		panel.add(btnNewButton);
		
		JButton btnCambiarColor = new JButton("Seleccionar Color");
		btnCambiarColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				Color colorSeleccionado = JColorChooser.showDialog(null, "Selecciona color", Color.BLUE);
				if(colorSeleccionado != null) {
					modificarColor(colorSeleccionado);
				}
			}
		});
		panel.add(btnCambiarColor);
		
		panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		
		JLabel lblNewLabel = new JLabel("New label");
		panel_1.add(lblNewLabel);
		
		panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.CENTER);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		panel_2.add(lblNewLabel_1);
		
		textField = new JTextField();
		panel_2.add(textField);
		textField.setColumns(10);
		
		btnNewButton_1 = new JButton("New button");
		panel_2.add(btnNewButton_1);
	}

	protected void modificarColor(Color colorSeleccionado) {
		btnNewButton.setBackground(colorSeleccionado);
		panel.setBackground(colorSeleccionado);
		panel_1.setBackground(colorSeleccionado);
		panel_2.setBackground(colorSeleccionado);
		btnNewButton_1.setBackground(colorSeleccionado);
		textField.setForeground(colorSeleccionado);
		
	}

}
