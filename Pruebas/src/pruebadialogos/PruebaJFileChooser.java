package pruebadialogos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.JFileChooser;

public class PruebaJFileChooser {
	
	
	public static void main(String[] args) {
		
		JFileChooser selector = new JFileChooser();
		
		int seleccion = selector.showSaveDialog(null);
		
		if(seleccion == JFileChooser.APPROVE_OPTION) {
			File fichero = selector.getSelectedFile();
			
			
			try {
				PrintWriter escritor = new PrintWriter(fichero);
				escritor.println("Hola");
				escritor.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
	}
}
