package purbacomboylist;

import javax.swing.JPanel;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;

public class PersonaRenderer extends JPanel implements ListCellRenderer<Persona>{

	private static final long serialVersionUID = 1L;
	private JTextField txtNombre;
	private JTextField txtApellidos;

	/**
	 * Create the panel.
	 */
	public PersonaRenderer() {
		
		initUI();

	}
	
	public void initUI(){
		JLabel lblNewLabel = new JLabel("Nombre:");
		add(lblNewLabel);
		
		txtNombre = new JTextField();
		add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Apellidos");
		add(lblNewLabel_1);
		
		txtApellidos = new JTextField();
		add(txtApellidos);
		txtApellidos.setColumns(10);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends Persona> arg0, Persona persona, int arg2, boolean selected,
			boolean arg4) {
		
		if(persona != null) {
			txtNombre.setText(persona.getNombre());
			txtApellidos.setText(persona.getApellidos());
		} else {
			txtNombre.setText("");
			txtApellidos.setText("");
		}
		
		if(selected) {
			this.setBackground(arg0.getSelectionBackground());
			this.setForeground(arg0.getSelectionForeground());
		} else {
			this.setBackground(arg0.getBackground());
			this.setForeground(arg0.getForeground());
		}
		
		return this;
	}

}
