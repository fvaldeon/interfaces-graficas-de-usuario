package purbacomboylist;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Vista extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JButton btnNewButton_2;
	private JButton btnNewButton_3;
	private JList<Persona> list;
	private JComboBox<Persona> comboBox;
	
	private DefaultListModel<Persona> dlm;
	private DefaultComboBoxModel<Persona> dcbm;
	
	/**
	 * Create the frame.
	 */
	public Vista() {
		initComponents();
		
		dlm = new DefaultListModel<Persona>();
		dcbm = new DefaultComboBoxModel<Persona>();
		list.setModel(dlm);
		
		list.setCellRenderer(new PersonaRenderer());
		
		comboBox.setModel(dcbm);
		
		comboBox.setRenderer(new PersonaRenderer());
		
		
		
	}


	private void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 554, 436);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setBounds(10, 12, 74, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Apellidos");
		lblNewLabel_1.setBounds(10, 37, 74, 14);
		contentPane.add(lblNewLabel_1);
		
		textField = new JTextField();
		textField.setBounds(94, 9, 149, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(94, 34, 149, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		btnNewButton = new JButton("A\u00F1adir a JList");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Persona persona = new Persona(textField.getText(), textField_1.getText());
				dlm.addElement(persona);
				
			}
		});
		btnNewButton.setBounds(10, 67, 118, 23);
		contentPane.add(btnNewButton);
		
		btnNewButton_1 = new JButton("Eliminar de JList");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				eliminarDeLista();
				
				
			}
		});
		btnNewButton_1.setBounds(10, 101, 118, 23);
		contentPane.add(btnNewButton_1);
		
		btnNewButton_2 = new JButton("A\u00F1adir a Combo");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Persona persona = new Persona(textField.getText(), textField_1.getText());
				dcbm.addElement(persona);
				
			}
		});
		btnNewButton_2.setBounds(138, 67, 127, 23);
		contentPane.add(btnNewButton_2);
		
		btnNewButton_3 = new JButton("Eliminar de Combo");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				eliminarDeComboBox();
				
			}
		});
		btnNewButton_3.setBounds(138, 101, 127, 23);
		contentPane.add(btnNewButton_3);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 149, 485, 115);
		contentPane.add(scrollPane);
		
		list = new JList<Persona>();
		list.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				
				//No compruebo la tecla pulsada
					eliminarDeLista();
				
				
			}
		});
		scrollPane.setViewportView(list);
		
		comboBox = new JComboBox<Persona>();
		comboBox.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				
				//Compruebo que la tecla es suprimir
				if(arg0.getKeyCode() == KeyEvent.VK_DELETE) {
				
					eliminarDeComboBox();
				}
				
			}
		});
		comboBox.setBounds(10, 290, 485, 45);
		contentPane.add(comboBox);
	}
	
	
	private void eliminarDeLista() {
		Persona persona = list.getSelectedValue();
		dlm.removeElement(persona);
	}

	private void eliminarDeComboBox() {
		Persona persona = (Persona) comboBox.getSelectedItem();
		dcbm.removeElement(persona);
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vista frame = new Vista();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
