# Desarrollo de Interfaces #
## Bloques 1-3 : Interfaces Gr�ficas de Usuario ##

Respositorio de ejercicios realizados en clase


 * ** Pruebas **: Proyecto en el que se plantean diferentes ejemplos en cada uno de sus packages
    * Package pruebacolorchooser: ejemplo de uso del dialogo JColorChooser para modificar el color de la gui
    * Package pruevadialogos: ejemplos de uso de los dialogos JOptionPane y JFileChooser
    * Package pruebacomboylist: aplicaci�n de ejemplo de uso de JComboBox y JList con sus respectivos Models
 * ** GestionAlumnos **: Proyecto de Gesti�n de alumnos. Ventana y Eventos, Organizar c�digo. Alta, eliminar, listar. Calendario
 * ** RegistroCoches **: Ejercicio en clase de Aplicacion MVC realizada en dos horas
 * ** GestionVehiculos **: Aplicacion MVC, utilizando layouts, iconos, cuadros de dialogo, JList, JComboBox, eventos de teclado, ventana, lista, accion. Se plantean sobre este proyecto todos los nuevos aspectos del temario.
