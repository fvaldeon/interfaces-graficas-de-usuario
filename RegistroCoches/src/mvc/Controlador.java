package mvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import base.Coche;

public class Controlador implements ActionListener{

	private Vista vista;
	private Modelo modelo;
	
	public Controlador(Vista vista, Modelo modelo) {
		this.vista = vista;
		this.modelo = modelo;
		
		vista.btnEliminar.addActionListener(this);
		vista.btnNuevo.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("listener activado");
		
		String comando = e.getActionCommand();
		
		switch(comando) {
		case "Nuevo":
			String matricula = vista.txtMatricula.getText();
			String modelo = vista.txtModelo.getText();
			double kms = Double.parseDouble(vista.txtKilometros.getText());
			LocalDate fechaFabricacion = vista.datePicker.getDate();
			
			this.modelo.altaCoche(matricula, modelo, kms, fechaFabricacion);
			break;
		case "Eliminar":
			Coche coche = vista.listaCoches.getSelectedValue();
			this.modelo.eliminarCoche(coche);
			break;
		}
		
		refrescarListaCoches();
	}
	
	private void refrescarListaCoches() {
		vista.dlm.clear();
		for(Coche coche : modelo.getCoches()) {
			vista.dlm.addElement(coche);
		}
	}
	
	


	
	
	
}
