package mvc;

import java.time.LocalDate;
import java.util.HashSet;

import base.Coche;

public class Modelo {

	private HashSet<Coche> coches;
	
	public Modelo() {
		coches = new HashSet<Coche>();
	}
	
	public HashSet<Coche> getCoches(){
		return coches;
	}
	
	public void altaCoche(String matricula, String modelo, double kms, LocalDate fechaFabricacion){
		coches.add(new Coche(matricula, modelo, kms, fechaFabricacion));
	}
	
	public void eliminarCoche(Coche eliminado) {
		coches.remove(eliminado);
	}
	
}
