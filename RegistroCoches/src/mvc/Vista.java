package mvc;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.github.lgooddatepicker.components.DatePicker;

import base.Coche;

public class Vista extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	JTextField txtMatricula;
	JTextField txtModelo;
	JTextField txtKilometros;
	JButton btnNuevo;
	JButton btnEliminar;
	DatePicker datePicker;
	
	//Otro Componente para listar elementos
	JList<Coche> listaCoches;
	DefaultListModel<Coche> dlm;


	public Vista() {
		initComponents();
	}




	private void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 424);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMatricula = new JLabel("Matricula");
		lblMatricula.setBounds(12, 12, 173, 15);
		contentPane.add(lblMatricula);
		
		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(12, 39, 173, 15);
		contentPane.add(lblModelo);
		
		JLabel lblKilometros = new JLabel("Kilometros");
		lblKilometros.setBounds(12, 66, 173, 15);
		contentPane.add(lblKilometros);
		
		JLabel lblFechaFabricacin = new JLabel("Fecha Fabricación");
		lblFechaFabricacin.setBounds(12, 93, 173, 15);
		contentPane.add(lblFechaFabricacin);
		
		txtMatricula = new JTextField();
		txtMatricula.setBounds(216, 10, 187, 19);
		contentPane.add(txtMatricula);
		txtMatricula.setColumns(10);
		
		txtModelo = new JTextField();
		txtModelo.setBounds(216, 37, 187, 19);
		contentPane.add(txtModelo);
		txtModelo.setColumns(10);
		
		txtKilometros = new JTextField();
		txtKilometros.setBounds(216, 64, 187, 19);
		contentPane.add(txtKilometros);
		txtKilometros.setColumns(10);
		
		datePicker = new DatePicker();
		datePicker.setBounds(216, 90, 212, 23);
		contentPane.add(datePicker);
		
		btnNuevo = new JButton("Nuevo");
		btnNuevo.setBounds(12, 120, 117, 25)	
		/**
		 * 
		 */;
		contentPane.add(btnNuevo);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(12, 157, 117, 25);
		contentPane.add(btnEliminar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 194, 416, 140);
		contentPane.add(scrollPane);
		
		listaCoches = new JList<Coche>();
		scrollPane.setViewportView(listaCoches);
		dlm = new DefaultListModel<Coche>();
		listaCoches.setModel(dlm);
		
		
		setLocationRelativeTo(null);
		setVisible(true);
	}
}
