package ventana;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.github.lgooddatepicker.components.DatePicker;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class Ventana extends JFrame {

	private static final long serialVersionUID = 1492798715489587143L;

	JTextField txtDni;
	JTextField txtNombre;
	JButton btnAlta;
	JButton btnEliminar;
	JButton btnListar;
	JTextArea txtAreaListar;
	DatePicker datePicker;
	JMenuItem menuItemGuardar;
	JMenuItem menuItemCargar;
	JMenuItem menuItemSalir;

	/**
	 * Create the frame.
	 */
	public Ventana() {
		//inicio los componentes gráficos
		initComponents();

	}


	/**
	 * Metodo que inicia los componentes graficos de Swing
	 */
	private void initComponents() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 451, 381);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		menuItemGuardar = new JMenuItem("Guardar");
		mnArchivo.add(menuItemGuardar);
		
		menuItemCargar = new JMenuItem("Cargar");
		mnArchivo.add(menuItemCargar);
		
		JMenu mnFichero = new JMenu("Ayuda\n");
		menuBar.add(mnFichero);
		
		menuItemSalir = new JMenuItem("Salir");
		mnFichero.add(menuItemSalir);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblDni = new JLabel("DNI");
		lblDni.setBounds(12, 12, 168, 15);
		contentPane.add(lblDni);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(12, 39, 168, 15);
		contentPane.add(lblNombre);

		JLabel lblFechaNacimiento = new JLabel("Fecha Nacimiento");
		lblFechaNacimiento.setBounds(12, 66, 168, 15);
		contentPane.add(lblFechaNacimiento);

		txtDni = new JTextField();
		txtDni.setBounds(184, 10, 204, 19);
		contentPane.add(txtDni);
		txtDni.setColumns(10);

		txtNombre = new JTextField();
		txtNombre.setBounds(184, 37, 204, 19);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);

		btnAlta = new JButton("Alta");
		btnAlta.setActionCommand("altaAlumno");
		btnAlta.setBounds(12, 95, 117, 25);
		contentPane.add(btnAlta);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.setActionCommand("eliminarAlumno");
		btnEliminar.setBounds(158, 95, 117, 25);
		contentPane.add(btnEliminar);

		btnListar = new JButton("Listar");
		btnListar.setActionCommand("listarAlumnos");
		btnListar.setBounds(299, 95, 117, 25);
		contentPane.add(btnListar);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 132, 394, 182);
		contentPane.add(scrollPane);

		txtAreaListar = new JTextArea();
		txtAreaListar.setEditable(false);
		scrollPane.setViewportView(txtAreaListar);
		
		datePicker = new DatePicker();
		datePicker.setBounds(176, 63, 212, 23);
		contentPane.add(datePicker);
	}
}
